package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/v22/daemon"
)

func getenv(key, defaultValue string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return defaultValue
}

type headerList map[string][]string

func (l headerList) String() string {
	var out []string
	for hdr, values := range l {
		for _, value := range values {
			out = append(out, fmt.Sprintf("%s: %s", hdr, value))
		}
	}
	return strings.Join(out, "; ")
}

func (l headerList) Set(value string) error {
	parts := strings.SplitN(value, ": ", 2)
	if len(parts) != 2 {
		return errors.New("invalid HTTP header format")
	}
	l[parts[0]] = append(l[parts[0]], parts[1])
	return nil
}

var (
	addr        = flag.String("addr", getenv("ADDR", ":3000"), "TCP `addr` to listen on")
	rootDir     = flag.String("root", getenv("ROOT_DIR", "/var/www"), "root `path` of the static content")
	httpHost    = flag.String("http-host", getenv("HTTP_HOST", ""), "if set, only serve requests matching this HTTP `host`")
	redirectURL = flag.String("redirect-url", getenv("REDIRECT_URL", ""), "redirect to this URL when / is called. If not set a blank page is shown")

	customHeaders headerList
)

func init() {
	customHeaders = make(headerList)
	flag.Var(&customHeaders, "header", "set arbitrary HTTP headers an all responses (may be specified multiple times)")
}

func buildStaticServer(dir, host string, headers headerList) http.Handler {
	// Setup the HTTP handler. If --http-host is set, add the
	// relevant Host check.
	h := http.FileServer(http.Dir(dir))
	if len(headers) > 0 {
		customH := h
		h = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			hdr := w.Header()
			for name, values := range headers {
				for _, value := range values {
					hdr.Set(name, value)
				}
			}
			customH.ServeHTTP(w, r)
		})
	}
	if host != "" {
		hostH := h
		h = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Header.Get("Host") != host {
				http.NotFound(w, r)
			}
			hostH.ServeHTTP(w, r)
		})
	}

	return h
}

// http.FileServer automatically creates a directory listing. We don't want that
func disableDirectoryListing(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		dir := path.Join(*rootDir, r.URL.Path)
		stat, err := os.Stat(dir)
		if errors.Is(err, os.ErrNotExist) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err != nil {
			log.Println("ERROR", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if stat.Mode().IsDir() {
			if *redirectURL == "" {
				// show blank page
				w.WriteHeader(http.StatusOK)
			} else {
				http.Redirect(w, r, *redirectURL, http.StatusFound)
			}
		} else {
			// serve files
			next.ServeHTTP(w, r)
		}
	})
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Sanity check: the root directory must exist.
	if _, err := os.Stat(*rootDir); os.IsNotExist(err) {
		log.Fatalf("root directory %s does not exist", *rootDir)
	}

	if *redirectURL != "" {
		if !strings.HasPrefix(*redirectURL, "http") {
			log.Fatalf("Redirect URL needs to start with https")
		}
	}

	h := buildStaticServer(*rootDir, *httpHost, customHeaders)
	http.Handle("/", disableDirectoryListing(h))

	// Start the HTTP server, and set it up to terminate
	// gracefully on SIGINT/SIGTERM.
	srv := &http.Server{
		Addr:         *addr,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  600 * time.Second,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		// Gracefully terminate for 3 seconds max, then shut
		// down remaining clients.
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err := srv.Shutdown(ctx)
		if err == context.Canceled {
			err = srv.Close()
		}
		if err != nil {
			log.Printf("error terminating server: %v", err)
		}
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	daemon.SdNotify(false, "READY=1")

	log.Printf("starting static server on %s", *addr)
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("error: %v", err)
	}
}
