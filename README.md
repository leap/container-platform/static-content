static-content
===

Tiny Docker image to serve static content over HTTP.

Sometimes you just need to serve static content (HTML, Javascript,
etc) over HTTP, and
[apache2-base](https://git.autistici.org/ai3/docker/apache-base) and
even Caddy are a bit too much for the task at hand. You can use this
Docker image, which is just a tiny Go HTTP server that does enough
HTTP to be useful (etag/range support, autoindexing, etc).

The full list of functionality is short:

* supports *If-Modified-Since* and *If-Match* request headers
* supports partial requests with the *Range* header
* it will automatically render HTML index pages for directories

You can set additional HTTP headers to be set on all responses (useful
for CORS, etc) with the *--header* command-line option.

